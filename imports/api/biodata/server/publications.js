import { Meteor } from 'meteor/meteor';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';

import { Biodata } from '../biodata.js';
import { BiodataFitness } from '../biodata.js';

/* eslint-disable */
Meteor.publish('biodata', function(userId) {
/* eslint-enable */
  new SimpleSchema({
    userId: { type: String },
  }).validate({ userId });

  return Biodata.find({ userId });
});

Meteor.publish('biodataFitness', function(userId) {
  new SimpleSchema({
    userId: { type: String },
  }).validate({ userId });

  return BiodataFitness.find({ userId });
});
