import { Mongo } from 'meteor/mongo';

export const Biodata = new Mongo.Collection('biodata');
export const BiodataFitness = new Mongo.Collection('biodataFitness');

// export const conn = DDP.connect('http://localhost:3000');
// export const Users = new Mongo.Collection('users', conn);
