import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';

import { Biodata } from '../imports/api/biodata/biodata.js';
import { BiodataFitness } from '../imports/api/biodata/biodata.js';

// Biodata for health
export const insertBiodata = new ValidatedMethod({
  name: 'biodata.insert',
  validate: new SimpleSchema({
    // heartBeat: { type: Number, decimal: true },
    // temperature: { type: Number, decimal: true },
    // respiratoryRate: { type: Number, decimal: true },
    // respiratoryVolume: { type: Number, decimal: true },
    // speed: { type: Number, decimal: true },
    // calories: { type: Number, decimal: true },
    // numberOfSteps: { type: Number, decimal: true },
    // userId: { type: String },
    // username:{ type: String },
    // duration: { type: Number, decimal: true},
    distance: { type: Number, decimal: true },
    vo2max: { type: Number, decimal: true },
    mas: { type: Number, decimal: true },
    // lactateConcentration: { type: Number },
    HRmax: { type: Number, decimal: true },
    HRrest: { type: Number, decimal: true },
    AT1: { type: Number, decimal: true },
    AT2: { type: Number, decimal: true },
    startDate: { type: String },
    endDate: { type: String },
    createdAt: { type: Date },
  }).validator(),
  run(newBiodata) {
    // if (! this.userId) {
    //   console.log('not-authorized');
    // }else{
    Biodata.insert(newBiodata);
  // }
  },
});

export const updateBiodata = new ValidatedMethod({
  name: 'biodata.update',
  validate: new SimpleSchema({
    biodataId: { type: String },
    // id: { type: String },
    // heartRate: { type: Number },
    // temperature: { type: Number },
    // respiratoryRate: { type: Number },
    // respiratoryVolume: { type: Number },
    // speed: { type: Number },
    // callories: { type: Number },
    // numberOfSteps: { type: Number },
    distance: { type: Number, decimal: true },
    vo2max: { type: Number, decimal: true },
    mas: { type: Number, decimal: true },
    // lactateConcentration: { type: Number },
    AT1: { type: Number, decimal: true },
    AT2: { type: Number, decimal: true },
    HRmax: { type: Number, decimal: true },
    HRrest: { type: Number, decimal: true },
    startDate: { type: String },
    endDate: { type: String },
    updatedAt: { type: Date },
  }).validator(),
  run(biodata) {
    Biodata.update({ _id: biodata.biodataId }, {
      $set: {
        distance: biodata.distance,
        vo2max: biodata.vo2max,
        mas: biodata.mas,
        AT1: biodata.AT1,
        AT2: biodata.AT2,
        HRmax: biodata.HRmax,
        HRrest: biodata.HRrest,
        startDate: biodata.startDate,
        endDate: biodata.endDate,
        updatedAt: biodata.updatedAt

      },
    });

  },
});

export const removeBiodata = new ValidatedMethod({
  name: 'biodata.remove',
  validate: new SimpleSchema({
    biodataId: { type: String },
  }).validator(),
  run({ biodataId }) {
    Biodata.remove(biodataId);
  },
});

// Biodata for fitness
export const insertBiodataFitness = new ValidatedMethod({
  name: 'biodataFitness.insert',
  validate: new SimpleSchema({
    weight: { type: Number, decimal: true },
    bmi: { type: Number, decimal: true },
    day: { type: Date },
  }).validator(),
  run(newBiodataFitness) {
    Biodata.insert(newBiodataFitness);
  },
});

export const updateBiodataFitness = new ValidatedMethod({
  name: 'biodataFitness.update',
  validate: new SimpleSchema({
    biodataFitnessId: { type: String },
    weight: { type: Number, decimal: true },
    bmi: { type: Number, decimal: true },
    day: { type: Date },
  }).validator(),
  run(biodataFitness) {
    Biodata.update({ _id: biodataFitness.biodataFitnessId }, {
      $set: {
        weight: biodataFitness.weight,
        bmi: biodataFitness.bmi,
        day: biodataFitness.day
      },
    });

  },
});

export const removeBiodataFitness = new ValidatedMethod({
  name: 'biodataFitness.remove',
  validate: new SimpleSchema({
    biodataFitnessId: { type: String },
  }).validator(),
  run({ biodataFitnessId }) {
    Biodata.remove(biodataFitnessId);
  },
});
